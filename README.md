Packaging API
======
This project represents a library that provides an API which helps to determine and pack items in a package by price and weight.
Accepts the absolute path to a file as a String. The file should be in UTF-8 format. The pack
method returns the solution as a String. Throws an com.mobiquity.exception.APIException if incorrect parameters are being passed.

### Solution
Challenge represents classical Knapsack problem with fractional input parameters. As the input size is considerably small brute-force search was 
chosen as the solution for this problem. This solution consists of systematically enumerating all possible candidates for the solution and checking whether each 
candidate satisfies the problem's statement.

### Constraints
* Max weight that a package can take is <= 100
* There might be up to 15 items you need to choose from
* Max weight and cost of an item is <= 100

### Example of input file
```bash
81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)
8 : (1,15.3,€34)
75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)
56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)
```

### Example of output
```bash
4
-
2,7
8,9
```