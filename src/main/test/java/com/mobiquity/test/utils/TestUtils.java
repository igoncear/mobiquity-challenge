package com.mobiquity.test.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class TestUtils {

    public static final String TEST_RESOURCES_PATH = "src/main/test/resources/";

    public static String readTestResource(String filePath) {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
            }
            return stringBuilder.toString().strip();
        } catch (IOException e) {
            log.error("Error on reading file.");
            return "";
        }
    }

}
