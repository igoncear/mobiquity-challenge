package com.mobiquity.utils;

import static com.mobiquity.test.utils.TestUtils.TEST_RESOURCES_PATH;
import static com.mobiquity.test.utils.TestUtils.readTestResource;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.mobiquity.exception.APIException;
import com.mobiquity.model.Item;
import com.mobiquity.model.Package;
import java.math.BigDecimal;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

class ParserTest {

    @Test
    @SneakyThrows
    void parsePackageTest() {
        var inputLine = readTestResource(TEST_RESOURCES_PATH + "parser_test_input");
        var result = Parser.parsePackage(inputLine);
        var expectedItems = asList(
            new Item(1, BigDecimal.valueOf(14.49), BigDecimal.valueOf(64)),
            new Item(2, BigDecimal.valueOf(26.99), BigDecimal.valueOf(64))
        );
        var expected = new Package(BigDecimal.ZERO, BigDecimal.valueOf(50), BigDecimal.ZERO, expectedItems);
        assertEquals(expected, result);
    }

    @Test
    @SneakyThrows
    void parseInvalidLineFormatTest() {
        var inputLine = readTestResource(TEST_RESOURCES_PATH + "invalid_line_format_test_input");
        assertThrows(APIException.class, () -> Parser.parsePackage(inputLine));
    }

    @Test
    @SneakyThrows
    void parseWrongPackageWeightTest() {
        var inputLine = readTestResource(TEST_RESOURCES_PATH + "wrong_package_weight_test_input");
        assertThrows(APIException.class, () -> Parser.parsePackage(inputLine));
    }

    @Test
    @SneakyThrows
    void parseWrongNumberOfItemsTest() {
        var inputLine = readTestResource(TEST_RESOURCES_PATH + "wrong_number_of_items_test_input");
        assertThrows(APIException.class, () -> Parser.parsePackage(inputLine));
    }

    @Test
    @SneakyThrows
    void parseWrongItemCostTest() {
        var inputLine = readTestResource(TEST_RESOURCES_PATH + "wrong_item_cost_test_input");
        assertThrows(APIException.class, () -> Parser.parsePackage(inputLine));
    }

    @Test
    @SneakyThrows
    void parseWrongItemWeightTest() {
        var inputLine = readTestResource(TEST_RESOURCES_PATH + "wrong_item_weight_test_input");
        assertThrows(APIException.class, () -> Parser.parsePackage(inputLine));
    }
}