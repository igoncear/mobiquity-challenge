package com.mobiquity.utils;

import static java.math.RoundingMode.HALF_DOWN;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.mobiquity.exception.APIException;
import com.mobiquity.model.Item;
import com.mobiquity.model.Package;
import com.mobiquity.packer.Packer;
import java.math.BigDecimal;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

class ReaderTest {

    private static final String TEST_RESOURCES_PATH = "src/main/test/resources/";

    @Test
    @SneakyThrows
    void readTest() {
        var result = Reader.read(TEST_RESOURCES_PATH + "reader_test_input");
        var expectedItems = asList(
            new Item(1, BigDecimal.valueOf(33.80).setScale(2, HALF_DOWN), BigDecimal.valueOf(40)),
            new Item(2, BigDecimal.valueOf(90.72), BigDecimal.valueOf(13)),
            new Item(3, BigDecimal.valueOf(6.76), BigDecimal.valueOf(64))
        );
        var expected = singletonList(new Package(BigDecimal.ZERO, BigDecimal.valueOf(56), BigDecimal.ZERO, expectedItems));
        assertEquals(expected, result);
    }

    @Test
    @SneakyThrows
    void readInvalidFilePathTest() {
        assertThrows(APIException.class, () -> Packer.pack( "/invalid_file_path"));
    }
}