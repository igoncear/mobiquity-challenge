package com.mobiquity.packer;

import static com.mobiquity.test.utils.TestUtils.TEST_RESOURCES_PATH;
import static com.mobiquity.test.utils.TestUtils.readTestResource;
import static org.junit.jupiter.api.Assertions.assertEquals;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
class PackerTest {

    private static final String ABSOLUTE_PATH_EXAMPLE_INPUT = "C:\\Projects\\challenges\\mobiquity\\src\\main\\test\\resources\\example_input";

    @Test
    @SneakyThrows
    void packTest() {
        var result = Packer.pack(ABSOLUTE_PATH_EXAMPLE_INPUT);
        var expected = readTestResource(TEST_RESOURCES_PATH + "example_output");
        assertEquals(expected, result);
    }

    @Test
    @SneakyThrows
    void packMultipleItemsTest() {
        var result = Packer.pack(TEST_RESOURCES_PATH + "multiple_items_packed_test_input");
        var expected = readTestResource(TEST_RESOURCES_PATH + "multiple_items_packed_test_output");
        assertEquals(expected, result);
    }

}