package com.mobiquity.packer;

import static com.mobiquity.model.Package.empty;
import static com.mobiquity.utils.Reader.read;
import static java.lang.String.valueOf;
import static java.math.BigDecimal.ZERO;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.joining;
import static org.junit.platform.commons.util.StringUtils.isBlank;

import com.mobiquity.exception.APIException;
import com.mobiquity.model.Item;
import com.mobiquity.model.Package;
import java.util.ArrayList;
import java.util.List;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Packer {

  private static final String NO_MATCH_PACKAGING_RESULT = "-";
  private static final String ITEMS_DELIMITER = ",";

  /**
   *  <ul>
   *      <li>Reads the input from a file;</li>
   *      <li>Parses the input;</li>
   *      <li>Validates the parsed content;</li>
   *      <li>Combines items for each input line by the maximum package weight and highest items cost.</li>
   *  </ul>
   *
   * @param filePath absolute path to the file with package details.
   * @return a String representing the packaging for entire file. Each line represents the result for a separate package
   * containing numbers of packed items separated by coma, or a dash if no package result was provided for input line.
   * @throws APIException if validation fails.
   */
  public static String pack(final String filePath) throws APIException {
    return read(filePath)
        .stream()
        .map(Packer::packSinglePackage)
        .map(packagingResult -> isBlank(packagingResult) ? NO_MATCH_PACKAGING_RESULT : packagingResult)
        .collect(joining(System.lineSeparator()));
  }

  /**
   * Receives the parsed package, calls the recursive package processing and formats the result.
   *
   * @param pack parsed package
   * @return numbers of packed items separated by coma
   */
  private static String packSinglePackage(final Package pack) {
    return packRecursive(empty(pack.getWeightLimit()), pack.getItems())
        .getItems().stream()
        .map(x -> valueOf(x.getNumber()))
        .collect(joining(ITEMS_DELIMITER));
  }

  /**
   * Recursive mechanism which combines all matched items by cost and max package weight.
   *
   * @param pack package which participates in comparison
   * @param items list of items for comparison by weight and cost
   * @return package with best combination of items by cost and package weight
   */
  private static Package packRecursive(final Package pack, final List<Item> items) {
    return items.stream()
        .map(item -> pack.hasSpaceFor(item)
            ? packRecursive(pack.addItem(item), truncateItemList(items, item))
            : pack)
        .max(comparing(Package::getCost).thenComparing(comparing(Package::getWeight).reversed()))
        .orElse(empty(ZERO));
  }

  /**
   * Method removes an item from the list.
   *
   * @param items list of items.
   * @param item item that should be removed.
   * @return new list of items.
   */
  private List<Item> truncateItemList(final List<Item> items, final Item item) {
    final List<Item> newList = new ArrayList<>(items);
    newList.remove(item);
    return newList;
  }

}
