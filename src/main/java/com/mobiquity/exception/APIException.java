package com.mobiquity.exception;

import org.slf4j.helpers.MessageFormatter;

public class APIException extends Exception {

  /**
   * @deprecated would not remove provided code
   */
  @Deprecated(forRemoval = true)
  public APIException(String message, Exception e) {
    super(message, e);
  }

  /**
   * @deprecated would not remove provided code
   */
  @Deprecated(forRemoval = true)
  public APIException(String message) {
    super(message);
  }

  public APIException(ExceptionType exceptionType, Object... messageArgs) {
    super(MessageFormatter.arrayFormat(exceptionType.getMessage(), messageArgs).getMessage());
  }

  public APIException(ExceptionType exceptionType, Exception e) {
    super(exceptionType.getMessage(), e);
  }
}
