package com.mobiquity.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The enum Exception type.
 */
@Getter
@AllArgsConstructor
public enum ExceptionType {

    ERROR_ON_READING_FILE("Error on reading file."),
    INPUT_LINE_HAS_WRONG_FORMAT("Input line doesn't match format: {}"),
    WEIGHT_OF_PACKAGE_EXCEEDS_CONSTRAINT("Weight of the package exceeds constraint: {}"),
    NUMBER_OF_ITEMS_EXCEEDS_MAX_VALUE("Number of items exceeds max value: {}"),
    WEIGHT_OF_ITEM_EXCEEDS_CONSTRAINT("Weight of an item exceeds constraint: {}"),
    COST_OF_ITEM_EXCEEDS_CONSTRAINT("Cost of an item exceeds constraint: {}");

    String message;

}
