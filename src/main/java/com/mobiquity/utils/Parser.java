package com.mobiquity.utils;

import static com.mobiquity.exception.ExceptionType.COST_OF_ITEM_EXCEEDS_CONSTRAINT;
import static com.mobiquity.exception.ExceptionType.INPUT_LINE_HAS_WRONG_FORMAT;
import static com.mobiquity.exception.ExceptionType.NUMBER_OF_ITEMS_EXCEEDS_MAX_VALUE;
import static com.mobiquity.exception.ExceptionType.WEIGHT_OF_ITEM_EXCEEDS_CONSTRAINT;
import static com.mobiquity.exception.ExceptionType.WEIGHT_OF_PACKAGE_EXCEEDS_CONSTRAINT;
import static java.util.Arrays.asList;
import static java.util.regex.Pattern.compile;

import com.mobiquity.exception.APIException;
import com.mobiquity.model.Item;
import com.mobiquity.model.Package;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.experimental.UtilityClass;

/**
 * Utility class for parsing input lines.
 */
@UtilityClass
class Parser {

    private static final String INPUT_LINE_FORMAT = "^(\\d+)(\\s+\\:\\s+)(\\(\\d+\\,\\d+\\.\\d+\\,\\€\\d+\\)\\s*)+";
    private static final String WEIGHT_ITEMS_DELIMITER = "(\\s\\:\\s)";
    private static final String ITEMS_DELIMITER = "(\\d+\\,\\d+\\.\\d+\\,\\€\\d+)";
    private static final String VALUES_DELIMITER = "(\\,\\€)|(\\,)";

    private static final BigDecimal MAX_PACKAGE_WEIGHT = new BigDecimal(100);
    private static final BigDecimal MAX_ITEM_WEIGHT = new BigDecimal(100);
    private static final BigDecimal MAX_ITEM_COST = new BigDecimal(100);
    private static final int MAX_NUMBER_OF_ITEMS = 15;

    /**
     * Parses input line into package using regex patterns.
     *
     * @param line input line.
     * @return parsed package.
     * @throws APIException in case when business constraints are violated.
     */
    static Package parsePackage(final String line) throws APIException {
        validateInputLine(line);

        final String[] tokens = line.split(WEIGHT_ITEMS_DELIMITER);
        final List<Item> packageItems = parseItems(tokens[1]);

        validatePackage(tokens[0]);
        validateItems(packageItems);
        return new Package(tokens[0], packageItems);
    }

    /**
     * Parses input String into list of items using regex patterns.
     *
     * @param items a String which contains items data.
     * @return list of parsed items.
     */
    private static List<Item> parseItems(final String items) {
        final Pattern pattern = compile(ITEMS_DELIMITER);
        final Matcher matcher = pattern.matcher(items);
        final List<Item> packageItems = new ArrayList<>();

        while (matcher.find()) {
            final List<String> values = asList(matcher.group().split(VALUES_DELIMITER));
            packageItems.add(new Item(values));
        }
        return packageItems;
    }

    /**
     * Method used for validation of business constraint related to items.
     *
     * @param packageItems package items.
     * @throws APIException in case when business constraints are violated.
     */
    private static void validateItems(final List<Item> packageItems) throws APIException {
        if (packageItems.size() > MAX_NUMBER_OF_ITEMS) {
            throw new APIException(NUMBER_OF_ITEMS_EXCEEDS_MAX_VALUE, MAX_NUMBER_OF_ITEMS);
        }
        if (packageItems.stream()
            .anyMatch(x -> x.getWeight().compareTo(MAX_ITEM_WEIGHT) > 0)) {
            throw new APIException(WEIGHT_OF_ITEM_EXCEEDS_CONSTRAINT, MAX_ITEM_WEIGHT);
        }
        if (packageItems.stream()
            .anyMatch(x -> x.getCost().compareTo(MAX_ITEM_COST) > 0)) {
            throw new APIException(COST_OF_ITEM_EXCEEDS_CONSTRAINT, MAX_ITEM_COST);
        }
    }

    /**
     * Method used for validation of business constraint related to package.
     *
     * @param weightLimit weight limit.
     * @throws APIException in case when business constraints are violated.
     */
    private static void validatePackage(final String weightLimit) throws APIException {
        if(new BigDecimal(weightLimit.strip()).compareTo(MAX_PACKAGE_WEIGHT) > 0) {
            throw new APIException(WEIGHT_OF_PACKAGE_EXCEEDS_CONSTRAINT, MAX_PACKAGE_WEIGHT);
        }
    }

    /**
     * Method used to validate if input line has right format.
     *
     * @param line input line.
     * @throws APIException when input line has wrong format or symbols.
     */
    private static void validateInputLine(final String line) throws APIException {
        if(!line.matches(INPUT_LINE_FORMAT)) {
            throw new APIException(INPUT_LINE_HAS_WRONG_FORMAT, INPUT_LINE_FORMAT);
        }
    }

}
