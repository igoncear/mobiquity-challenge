package com.mobiquity.utils;

import static com.mobiquity.exception.ExceptionType.ERROR_ON_READING_FILE;
import static com.mobiquity.utils.Parser.parsePackage;

import com.mobiquity.exception.APIException;
import com.mobiquity.model.Package;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.experimental.UtilityClass;

/**
 * Utility class for reading input files.
 */
@UtilityClass
public class Reader {

    /**
     * Read file line by line and send each line to line parser.
     *
     * @param filePath absolute path to the input file.
     * @return list of parsed packages.
     * @throws APIException when occurs problem with file reading.
     */
    public static List<Package> read(final String filePath) throws APIException {
        final List<Package> packageList = new ArrayList<>();
        try (FileReader fr = new FileReader(filePath);
            BufferedReader br = new BufferedReader(fr)) {
            String line;
            while ((line = br.readLine()) != null) {
                packageList.add(parsePackage(line));
            }
        } catch (IOException e) {
            throw new APIException(ERROR_ON_READING_FILE, e);
        }
        return packageList;
    }

}
