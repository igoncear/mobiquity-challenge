package com.mobiquity.model;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;
import static java.util.stream.Stream.of;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Package with items, weight limit, calculated item's weight and cost.
 */
@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
public class Package {

    private final BigDecimal weight;
    private final BigDecimal weightLimit;
    private final BigDecimal cost;
    private final List<Item> items;

    /**
     * Constructor used to create a reference Package containing all items.
     *
     * @param weightLimit weight limit from the input line
     * @param items items from input line
     */
    public Package(final String weightLimit, final List<Item> items) {
        this.weightLimit = new BigDecimal(weightLimit.strip());
        this.items = items;
        this.weight = BigDecimal.ZERO;
        this.cost = BigDecimal.ZERO;
    }

    /**
     * Creates an empty package of a specific weight limit.
     *
     * @param weightLimit weight limit.
     * @return new empty package.
     */
    public static Package empty(BigDecimal weightLimit) {
        return new Package(BigDecimal.ZERO, weightLimit, BigDecimal.ZERO, new ArrayList<>());
    }

    /**
     * Creates a new package on insertion of a new item. New package weight and cost are recalculated accordingly.
     *
     * @param item the item to be added.
     * @return new package as a result of merging.
     */
    public Package addItem(Item item) {
        return new Package(
            this.getWeight().add(item.getWeight()),
            this.weightLimit,
            this.getCost().add(item.getCost()),
            concat(this.getItems().stream(), of(item)).collect(toList()));
    }

    /**
     * Checks if package has enough space for the item insertion.
     *
     * @param item the item to check.
     * @return {@code true} if can fit into package, {@code false} otherwise.
     */
    public boolean hasSpaceFor(Item item) {
        return this.getWeight().add(item.getWeight()).compareTo(this.getWeightLimit()) <= 0;
    }

}
