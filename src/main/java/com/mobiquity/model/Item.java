package com.mobiquity.model;

import static java.lang.Integer.parseInt;

import java.math.BigDecimal;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Item with a number, weight and cost.
 */
@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
public class Item {

    private final int number;
    private final BigDecimal weight;
    private final BigDecimal cost;

    /**
     * Constructor used to create an Item after file parsing.
     *
     * @param values properties of an item each represented as a String.
     */
    public Item(final List<String> values) {
        this.number = parseInt(values.get(0));
        this.weight = new BigDecimal(values.get(1));
        this.cost = new BigDecimal(values.get(2));
    }

}
